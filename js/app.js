const button_div = document.getElementById("generate-button");
const image_textfield_div = document.getElementById("image-input");

const image_link_label = document.getElementById("img_link");
const meme_image_div = document.getElementById("test-img");

//Text fields input
const tf1_label = document.getElementById("tf1");
const blur_cb = document.getElementById("blur");
const bw_cb = document.getElementById("gray");

function testButtons(){
    console.log("The button works!");
    console.log("Textfield: " + tf1_label.value);
    console.log("Blur checkbox: " + blur_cb.checked);
    console.log("Gray checkbox: " + bw_cb.checked);

    let rest_call = buildRestCall();
    console.log("Rest: " + rest_call);
}

function changeImage(image_id){
    meme_image_div.src = "http://localhost:5000/api/get/images/".concat(image_id, ".jpg");
    console.log("http://localhost:5000/api/get/images/" + image_id + ".jpg");
    //image_link_label.value = "http://localhost:5000/api/get/images/" + image_id + ".jpg";

} 

function generateImage(){
    var image_id = Math.floor(Math.random() * 10000);
    //testButtons()
    dispatchRestCall((buildRestCall(image_id)));
    changeImage(image_id);
    //window.open(image_link_label.value);

}

function updateImage(){
    console.log("Updating the image.");
    console.log("Image link: " + meme_image_div.src);
    document.getElementById("test-img").src = image_link_label.value;
    //meme_image_div = "";
}

function buildRestCall(image_id){
    const backend_server = 'http://localhost:5000';
    return backend_server + '/api' + '/' + tf1_label.value + '/' + blur_cb.checked + '/' + bw_cb.checked + '/' + image_id + '/' + image_link_label.value;
}

function dispatchRestCall(rest_call){

    var i = 0;

    while(i < 10){
        fetch(rest_call);
        i++;
    }
    
    //.then(response => response.json())
}

function main(){
    button_div.addEventListener('click', () => generateImage());
    image_textfield_div.addEventListener('input', () => updateImage());
}

main();